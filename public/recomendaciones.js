
// JS
// Variables globales
let pagina = 1;      
let paginas = 2;
let carrusel = document.querySelector(".carrusel"); 
let indicador = document.querySelector(".indicador-pagina");

// Función para mover el carrusel a la izquierda
function moverIzquierda() {
    if (pagina > 1) {
        pagina--;
        // Se actualiza el indicador de página
        indicador.textContent = "Página " + pagina + " de " + paginas;
        // Se calcula el desplazamiento del carrusel en porcentaje
        let desplazamiento = - (pagina - 1) * 100;
        // Se aplica el desplazamiento al carrusel con una transformación CSS
        carrusel.style.transform = "translateX(" + desplazamiento + "%)";
    }
}

// Función para mover el carrusel a la derecha
function moverDerecha() {
    if (pagina < paginas) {
        pagina++;
        // Se actualiza el indicador de página
        indicador.textContent = "Página " + pagina + " de " + paginas;
        // Se calcula el desplazamiento del carrusel en porcentaje
        let desplazamiento = - (pagina - 1) * 100; // Se corrigió la fórmula
        // Se aplica el desplazamiento al carrusel con una transformación CSS
        carrusel.style.transform = "translateX(" + desplazamiento + "%)";
    }
}