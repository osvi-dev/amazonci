let cartContent = JSON.parse(localStorage.getItem("cart"));

function loadCart() {
  const container = document.getElementById("cartContainer");
  container.innerHTML = "";
  if (!cartContent || cartContent.length === 0) {
    container.innerHTML = `
    <div class="flex items-center justify-center mt-10">
        <div class="text-center">
        <h1 class="text-4xl font-bold mb-4">Tu carrito de Amazon está vacío</h1>
        <p class="text-sm text-gray-500 mb-8">Revisa tus artículos guardados para más tarde abajo o <a href="index.html" class="text-blue-500 hover:underline">continúa comprando</a>.</p>
        </div>
    </div>
    `;
    return;
  }
  cartContent.forEach((cartItem, index) => {
    const formattedPrice = parseFloat(cartItem.price.replace(/[^0-9.-]+/g, ""));
    const formattedQuantity = parseInt(cartItem.quantity);

    const totalPrice = formattedPrice * formattedQuantity;

    if (formattedQuantity > 0) {
      const productId = `product-${index}`;
      container.innerHTML += `
          <div id="${productId}" class="flex flex-col md:flex-row md:items-center md:justify-between w-full px-4 mb-4"> <!-- Envoltura en columna y clase mb-4 para espacio entre elementos -->
            <img class="w-32 h-32 object-cover md:mr-4 mb-4 md:mb-0" src="${
              cartItem.image
            }" alt="Nombre del producto">
            
            <div class="flex flex-col md:flex-row md:items-center md:justify-between w-full px-6">
                <div class="p-4">
                    <h2 class="text-xl font-semibold">${cartItem.name}</h2>
                    <p class="text-gray-600">Calificaciones: ${
                      cartItem.ratings
                    }</p>
                    <p class="text-gray-600">Fecha: ${cartItem.shippingDate}</p>
                    <div class="flex items-center">
                        <button data-product-id="${productId}" class="p-2 rounded-full bg-gray-200 mr-2 decrement-button">&lt;</button> <!-- Añadido el atributo de datos para identificar el producto -->
                        <div id="counter-${productId}" class="text-lg font-bold">${
        cartItem.quantity
      }</div> <!-- Añadido el identificador único del producto al id del contador -->
                        <button data-product-id="${productId}" class="p-2 rounded-full bg-gray-200 ml-2 increment-button">&gt;</button> <!-- Añadido el atributo de datos para identificar el producto -->
                    </div>
                </div>
            </div>
            
            <div class="text-right text-3xl font-bold self-start">
                <p id="price-${productId}" class="text-gray-600">${totalPrice.toLocaleString(
        "en-US",
        { style: "currency", currency: "USD" }
      )}</p> <!-- Mostrar el precio total con formato de moneda -->
            </div>
          </div>
        `;
    }
  });

  const incrementButtons = document.querySelectorAll(".increment-button");
  const decrementButtons = document.querySelectorAll(".decrement-button");
  incrementButtons.forEach((button) => {
    button.addEventListener("click", () =>
      updateCounter(button.dataset.productId, true)
    );
  });
  decrementButtons.forEach((button) => {
    button.addEventListener("click", () =>
      updateCounter(button.dataset.productId, false)
    );
  });
}

function updateCounter(productId, increment) {
  const counter = document.getElementById(`counter-${productId}`);
  let count = parseInt(counter.textContent);
  count = increment ? count + 1 : Math.max(0, count - 1);
  counter.textContent = count;

  const index = parseInt(productId.split("-")[1]);
  const formattedPrice = parseFloat(
    cartContent[index].price.replace(/[^0-9.-]+/g, "")
  );
  const totalPrice = formattedPrice * count;

  const totalPriceElement = document.getElementById(`price-${productId}`);
  totalPriceElement.textContent = totalPrice.toLocaleString("en-US", {
    style: "currency",
    currency: "USD",
  });

  cartContent[index].quantity = count;
  localStorage.setItem("cart", JSON.stringify(cartContent));

  if (count === 0) {
    cartContent.splice(index, 1);
    localStorage.setItem("cart", JSON.stringify(cartContent));
    loadCart();
  }
  loadSubtotal();
}

function loadSubtotal() {
    const container = document.getElementById('cartSubtotal');
  
    let subtotal = 0;
  
    if (cartContent && cartContent.length > 0) {
      subtotal = cartContent.reduce((acc, cartItem) => {
        const formattedPrice = parseFloat(cartItem.price.replace(/[^0-9.-]+/g, ""));
        return acc + (formattedPrice * cartItem.quantity);
      }, 0);
    }
  
    container.innerHTML = `
      <h1 class="text-2xl font-bold mb-4">Subtotal:</h1>
      <p class="text-3xl font-bold text-black mb-8">${subtotal.toLocaleString('en-US', { style: 'currency', currency: 'USD' })}</p>
      <button class="bg-yellow-500 hover:bg-yellow-600 text-white font-bold py-2 px-4 rounded">
          Proceder al pago
      </button>
    `;
  }

window.onload = function () {
  if (!cartContent) {
    cartContent = [];
  }
  loadCart();
  loadSubtotal();
};
